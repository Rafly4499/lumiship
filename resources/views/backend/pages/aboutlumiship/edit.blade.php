    @extends('backend.app')

    @section('content')
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-7 align-self-center">
                <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">About lumiship</h4>
                <div class="d-flex align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb m-0 p-0">
                            <li class="breadcrumb-item"><a href="{{ url('/panel/dashboard') }}"
                                    class="text-muted">Beranda</a></li>
                            <li class="breadcrumb-item"><a href="{{ url('/panel/about') }}" class="text-muted">About
                                    lumiship</a></li>
                            <li class="breadcrumb-item text-muted active" aria-current="page">Edit Data</li>
                        </ol>
                    </nav>
                </div>
            </div>

        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Edit Data About lumiship</h4>

                        <form action="{{ url('/panel/about/'.$about->id) }}" method="post"
                            enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">About lumiship </label>
                                <div class="col-md-12">
                                    <textarea class="form-control" rows="3" name="about">{{ $about->about  }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Address/Location</label>
                                <div class="col-md-12">
                                    <textarea class="form-control" rows="3"
                                        name="location">{{ $about->location  }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Email</label>
                                <div class="col-md-12">
                                    <input class="form-control" type="email" rows="3" value="{{ $about->email  }}"
                                        name="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Contact Person</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" value="{{ $about->contact_person }}"
                                        name="contact_person">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">No Handphone</label>
                                <div class="col-md-12">
                                    <input class="form-control" rows="3" value="{{ $about->no_hp }}" type="number" name="no_hp">
                                </div>
                            </div>



                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Link Facebook</label>
                                <div class="col-md-12">
                                    <input type="url" class="form-control" value="{{ $about->link_facebook }}"
                                        name="link_facebook">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Link Instagram</label>
                                <div class="col-md-12">
                                    <input type="url" class="form-control" value="{{ $about->link_instagram }}"
                                        name="link_instagram">
                                </div>
                            </div>
                            <button type="submit" name="submit" class="btn btn-info">SAVE</button>
                            <a href="{{ url('/panel/about') }}"><button type="button"
                                    class="btn btn-dark">CANCEL</button></a>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    @endsection