@extends('backend.app')

@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Identity Lumiship</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="/" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/panel/about') }}" class="text-muted">Identity
                                Lumiship</a></li>
                    </ol>
                </nav>
            </div>
        </div>

    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Data Identity lumiship</h4>
                    @if($item)

                    <div class="col-md-12">
                        <div class="row">

                            <label class="col-md-4">About</label>

                            <div class="col-md-8">
                                <p>: {!! $item->about !!}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">

                            <label class="col-md-4">Address/Location</label>

                            <div class="col-md-8">
                                <p>: {!! $item->location !!}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">

                            <label class="col-md-4">Email</label>

                            <div class="col-md-8">
                                <div class="detblog">: {{ $item->email }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">

                            <label class="col-md-4">Contact Person</label>

                            <div class="col-md-8">
                                <p>: {{ $item->contact_person }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">

                            <label class="col-md-4">No HP</label>

                            <div class="col-md-8">
                                <p>: {{ $item->no_hp }}</p>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="row">

                            <label class="col-md-4">Facebook</label>

                            <div class="col-md-8">
                                <p>: {{ $item->link_facebook }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">

                            <label class="col-md-4">Instagram</label>

                            <div class="col-md-8">
                                <p>: {{ $item->link_instagram }}</p>
                            </div>
                        </div>
                    </div>


                    <form action="{{ url('/panel/about/'.$item->id) }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                        <a href="{{ url('/panel/about/'.$item->id.'/edit') }}" class="btn btn-success"><i
                                class="fas fa-pencil-alt"></i> Edit</a>
                    </form>

                    @else
                    <a href="{{ url('/panel/about/create') }}"><button type="button"
                            class="btn waves-effect waves-light btn-primary"><i class="fa fa-plus"></i>Insert
                            Data</button></a>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection