@extends('backend.app') 
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Contact Message</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('/panel/dashboard') }}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Contact Message</li>
                    </ol>
                </nav>
            </div>
        </div>

    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Contact Message</h4>
                    <p>To reply messages, please enter the link <a href="https://mail.lumiship.co.id/"> mail.lumiship.co.id</a> or button reply by email cs@lumiship.co.id</p>
                    <div class="table-responsive">
                        <table id="default_order" class="table table-striped table-bordered display" style="width:100%">
                            <thead>
                                
                                <tr>
                                    <th>Date</th>
                                    <th>Name</th>
                                    <th>No Handphone</th>
                                    <th>Email</th>
                                    <th>Subject Pesan</th>
                                    <th>Message</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0;?> @foreach($contact as $item)
                                <?php $no++;?>
                                <tr>
                                    <td>{{  date('d M Y', strtotime($item->created_at)) }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->phone_number }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->subject }}</td>
                                    <td>{{ $item->message }}</td>
                                    <td>
                                        
                                        <form action="{{ url('/panel/contactus/'.$item->id) }}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <a href="mailto:{{ $item->email }}" class="btn btn-success"><i class="fas fa-reply"></i> Reply</a>
                                            <a href="{{ url('/panel/contactus/'.$item->id.'/view') }}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> </button>
                                        </form>
                                    </td>
                                </tr>

                                @endforeach

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Date</th>
                                    <th>Name</th>
                                    <th>No Handphone</th>
                                    <th>Email</th>
                                    <th>Subject Pesan</th>
                                    <th>Message</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
</div>
@endsection
