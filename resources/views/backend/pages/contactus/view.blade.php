@extends('backend.app')

@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Contact Message</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                    <li class="breadcrumb-item"><a href="/" class="text-muted">Beranda</a></li>
                                    <li class="breadcrumb-item"><a href="{{ url('/panel/contactus') }}" class="text-muted">Contact Message</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">View Data</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">View Data Contact Message</h4>
                                <a href="{{ url('/panel/contactus') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                               
                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Nama Lengkap</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$contact->name}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">No Handphone</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$contact->phone_number}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Subject Pesan</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$contact->subject}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Isi Pesan</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$contact->message}}</p> </div>
                                                    </div>
                                                </div>                                                                                                                                      
                            </div>
                        </div>
                    </div>
                    
                    </div>
                </div>
            </div>
            @endsection