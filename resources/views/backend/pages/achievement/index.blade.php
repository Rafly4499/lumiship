@extends('backend.app')

@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Achievement</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                    <li class="breadcrumb-item"><a href="{{ url('/panel/dashboard') }}" class="text-muted">Beranda</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">Achievement</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">List Achievement</h4>
                                @if(count($achievement) == 0)
                                <a href="{{ url('/panel/achievement/create') }}"><button type="button" class="btn waves-effect waves-light btn-primary"><i class="fa fa-plus"></i>Insert Data</button></a>
                                @endif
                                <div class="table-responsive">
                                    <table id="default_order" class="table table-striped table-bordered display no-wrap"
                                        style="width:100%">
                                        <thead>
                                            <tr>

                                                <th>Delivered Packages</th>
                                                <th>Countries Covered</th>
                                                <th>Project Good</th>
                                                <th>Satisfied Clients</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $no = 0;?>
                                        @foreach($achievement as $item)
                                        <?php $no++;?>
                                        <tr>
                                        
                                            <td>{{ $item->delivered_packages }}</td>
                                            <td>{{ $item->countries }}</td>
                                            <td>{{ $item->project }}</td>
                                            <td>{{ $item->client }}</td>
                                            
                                            <td>
                                                <form action="{{ url('/panel/achievement/'.$item->id) }}" method="post">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <a href="{{ url('/panel/achievement/'.$item->id.'/edit') }}" class="btn btn-success"><i class="fas fa-pencil-alt"></i> Edit</a>
                                                    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                        
                                        @endforeach
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                               
                                                <th>Client</th>
                                                <th>Project</th>
                                                <th>Experience Years</th>
                                                <th>Support</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            @endsection