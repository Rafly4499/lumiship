@extends('backend.app')
@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h3 class="page-title text-truncate text-dark font-weight-medium mb-1">Hello Admin Lumiship! Selamat Datang
            </h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-5 align-self-center">
            <div class="customize-input float-right">
                <select
                    class="custom-select custom-select-set form-control bg-white border-0 custom-shadow custom-radius">
            <option>{{date('d M Y ')}}</option>
                </select>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- *************************************************************** -->
    <!-- Start First Cards -->
    <!-- *************************************************************** -->
    <div class="row">
        <!-- Column -->
        <div class="col-md-6 col-lg-4 col-xlg-4">
            <a href="{{ url('/panel/product') }}">
            <div class="card card-hover">
                <div class="p-2 bg-orangered text-center">
                    <h1 class="font-light text-white">{{ $countproduct }}</h1>
                    <h6 class="text-white">Total Product Service</h6>
                </div>
            </div>
            </a>
        </div>

        <!-- Column -->
        <div class="col-md-6 col-lg-4 col-xlg-4">
            <a href="{{ url('/panel/theory') }}">
            <div class="card card-hover">
                <div class="p-2 bg-orange text-center">
                    <h1 class="font-light text-white">{{ $counttheory }}</h1>
                    <h6 class="text-white">Total Theory Logistic</h6>
                </div>
            </div>
        </a>
        </div>
        <div class="col-md-6 col-lg-4 col-xlg-4">
            <a href="{{ url('/panel/contactus') }}">
            <div class="card card-hover">
                <div class="p-2 bg-cyan text-center">
                    <h1 class="font-light text-white">{{ $countcontact }}</h1>
                    <h6 class="text-white">Total Contact Message</h6>
                </div>
            </div>
        </a>
        </div>
        <!-- Column -->

        <!-- Column -->
    </div>
    <div class="row">
        <div class="col-md-6 col-lg-8">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-start">
                        <h4 class="card-title mb-0">Earning Statistics</h4>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div id="chart-1-container"></div>
                        </div>
                        <div class="col-md-6">
                            <div id="chart-2-container"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4">
            <h4>Achievement</h4>
            @if($achieve)

            <div class="card border-right">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <div class="d-inline-flex align-items-center">
                                <h2 class="text-dark mb-1 font-weight-medium">{{ $achieve->client }}</h2>
                            </div>
                            <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Client Satisfied</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <img style="width:60px;"
                                src="{{ asset('assets/images/marketplace/005-businessman.png')}}" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="card border-right">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <h2 class="text-dark mb-1 w-100 text-truncate font-weight-medium">{{ $achieve->project }}
                            </h2>
                            <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Project
                            </h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <img style="width:60px;" src="{{ asset('assets/images/marketplace/004-hand shake.png')}}" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="card border-right">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <div class="d-inline-flex align-items-center">
                                <h2 class="text-dark mb-1 font-weight-medium">{{ $achieve->delivered_packages }}</h2>

                            </div>
                            <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Delivered Packages</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <img style="width:60px;"
                                src="{{ asset('assets/images/marketplace/030-online store.png')}}" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <h2 class="text-dark mb-1 font-weight-medium">{{ $achieve->countries }}</h2>
                            <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Countries Covered</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <img style="width:60px;" src="{{ asset('assets/images/marketplace/003-megaphone.png')}}" />
                        </div>
                    </div>
                </div>
            </div>
            @else

            <div class="card border-right">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <div class="d-inline-flex align-items-center">
                                <h2 class="text-dark mb-1 font-weight-medium">0</h2>
                            </div>
                            <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Client Satisfied</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <img style="width:60px;"
                                src="{{ asset('assets/images/marketplace/005-businessman.png')}}" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="card border-right">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <h2 class="text-dark mb-1 w-100 text-truncate font-weight-medium">0</h2>
                            <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Project
                            </h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <img style="width:60px;" src="{{ asset('assets/images/marketplace/004-hand shake.png')}}" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="card border-right">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <div class="d-inline-flex align-items-center">
                                <h2 class="text-dark mb-1 font-weight-medium">0</h2>

                            </div>
                            <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Delivered Package</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <img style="width:60px;"
                                src="{{ asset('assets/images/marketplace/030-online store.png')}}" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <h2 class="text-dark mb-1 font-weight-medium">0</h2>
                            <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Countries Covered</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <img style="width:60px;" src="{{ asset('assets/images/marketplace/003-megaphone.png')}}" />
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>

</div>

</div>
<script>
    gapi.analytics.ready(function() {
    gapi.analytics.auth.authorize({
        'serverAuth': {
        'access_token': "<?php echo $accesstoken; ?>"
        }
    });
​
    var dataChart1 = new gapi.analytics.googleCharts.DataChart({
        query: {
        'ids': 'ga:257738073', // <-- Replace with the ids value for your view.
        'start-date': '30daysAgo',
        'end-date': 'yesterday',
        'metrics': 'ga:sessions,ga:users',
        'dimensions': 'ga:date'
        },
        chart: {
        'container': 'chart-1-container',
        'type': 'LINE',
        'options': {
            'width': '100%'
        }
        }
    });
    dataChart1.execute();
​
    var dataChart2 = new gapi.analytics.googleCharts.DataChart({
        query: {
        'ids': 'ga:257738073', // <-- Replace with the ids value for your view.
        'start-date': '30daysAgo',
        'end-date': 'yesterday',
        'metrics': 'ga:pageviews',
        'dimensions': 'ga:pagePathLevel1',
        'sort': '-ga:pageviews',
        'filters': 'ga:pagePathLevel1!=/',
        'max-results': 7
        },
        chart: {
        'container': 'chart-2-container',
        'type': 'PIE',
        'options': {
            'width': '100%',
            'pieHole': 4/9,
        }
        }
    });
    dataChart2.execute();
​
    });
</script>
@endsection