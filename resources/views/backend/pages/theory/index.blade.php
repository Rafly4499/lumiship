@extends('backend.app')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Theory</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('/panel/dashboard') }}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">List Theory</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">List Theory</h4>
                    <a href="{{ url('/panel/theory/create') }}"><button type="button" class="btn waves-effect waves-light btn-primary"><i class="fa fa-plus"></i>  Insert Data</button></a>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Product Service</th>
                                    <th>Title File</th>
                                    
                                    <th>File</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0;?> @foreach($theory as $item)
                                <?php $no++;?>
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>{{ $item->category['title'] }}</td>
                                    <td>{{ $item->title }}</td>
                                    <td><a href="{{ url('upload/'.$item->file) }}" class="btn btn-orange"><i class="fas fa-download"></i> Download/Show File</a></td>
                                    <td>{!! $item->description !!}</td>
                                    <td>
                                        <form action="{{ url('/panel/theory/'.$item->id) }}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <a href="{{ url('/panel/theory/'.$item->id.'/edit') }}" class="btn btn-success"><i class="fas fa-pencil-alt"></i> Edit</a>
                                            <a href="{{ url('/panel/theory/'.$item->id.'/view') }}" class="btn btn-info"><i class="fas fa-eye"></i> View</a>
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>

                                        </form>
                                    </td>
                                </tr>

                                @endforeach

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Category</th>
                                    <th>Title File</th>
                                    <th>File</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
</div>
@endsection
