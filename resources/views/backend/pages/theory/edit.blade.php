@extends('backend.app')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Theory</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('/panel/dashboard') }}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/panel/theory') }}" class="text-muted">Theory</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Edit Data</li>
                    </ol>
                </nav>
            </div>
        </div>

    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Edit Data Theory</h4>
                    <a href="{{ url('/panel/theory') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                    <form action="{{ url('/panel/theory/'.$theory->id) }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Title File</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="title" value="{{ $theory->title }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Description</label>
                        <div class="col-md-12">
                            <textarea class="form-control" id="summary-ckeditor" rows="3" name="description">{{ $theory->description }}</textarea>
                        </div>
                    </div>
                    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
                    <script>
                        CKEDITOR.replace('summary-ckeditor');

                    </script>

<div class="col-md-12">
    <div class="form-group">
        <label for="fullname">Product Service</label>
        <select class="form-control select2" value="{{ $theory->category['id'] }}" name="product_id" id="" required>
            <option value="{{ $theory->category['id'] }}">{{ $theory->category['title'] }}</option>
            @foreach($category as $data)
                <option value="{{ $data->id }}">{{ $data->title }}</option>
            @endforeach
            </select>
    </div>
</div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">File theory</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Upload</span>
                                </div>
                                <div class="custom-file">
                                    <input type="file" name="file" class="custom-file-input" id="inputGroupFile01" data-default-file="{{ $theory->file }}">
                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                </div>
                            </div>
                            <label for="" class="col-sm-12 control-label">Masukkan File Dokumen Maksimal 2 Mb</label>
                        </div>
                            <button type="submit" name="submit" class="btn btn-info">SAVE</button>
                            <a href="{{ url('/panel/theory') }}"><button type="button" class="btn btn-dark">CANCEL</button></a>

                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
