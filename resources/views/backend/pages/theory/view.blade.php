@extends('backend.app')

@section('content')
<style>
    .dettheory img{
    width: 100% !important;
}
</style>
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Theory</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                    <li class="breadcrumb-item"><a href="/" class="text-muted">Beranda</a></li>
                                    <li class="breadcrumb-item"><a href="{{ url('/panel/theory') }}" class="text-muted">Theory</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">View Data</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">View Data Theory</h4>
                                <a href="{{ url('/panel/theory') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                               
                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Title</label>
                                                        
                                                        <div class="col-md-8"><p>: {{$theory->title}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Description</label>
                                                        
                                                        <div class="col-md-8">: {!! ($theory->description) !!}</div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Product Service</label>
                                                        
                                                        <div class="col-md-8"><p>: {{$theory->category['title']}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Slug theory</label>
                                                        
                                                        <div class="col-md-8"><p>: {{$theory->slug}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">File Theory</label>
                                                        
                                                        <div class="col-md-8">: <a href="{{ url('upload/'.$theory->file) }}" class="btn btn-orange"><i class="fas fa-download"></i> Download/Show File</a> </div>
                                                    </div>
                                                </div>
                                                
                                    
                                   <a href="{{ url('/panel/theory/'.$theory->id.'/edit') }}"><button type="submit" name="submit" class="btn btn-info">UPDATE</button></a> 
                                     
                                
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            @endsection