@extends('backend.app')

@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Product Services</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                    <li class="breadcrumb-item"><a href="{{ url('/panel/dashboard') }}" class="text-muted">Beranda</a></li>
                                    <li class="breadcrumb-item"><a href="{{ url('/panel/product') }}" class="text-muted">Product Services</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">Edit Data</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Edit Data Product Services</h4>
                               
                                <form action="{{ url('/panel/product/'.$product->id) }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="PUT">
                
                                    <div class="form-group">
                                        <label for="" class="col-sm-2 control-label">Title</label>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control" value="{{ $product->title }}" name="title">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Description</label>
                                    <div class="col-md-12">
                                        <textarea class="form-control" rows="3" name="description">{{ $product->description }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Photo</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Upload</span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" name="image" class="custom-file-input" id="inputGroupFile01" data-default-file="{{ $product->photo }}">
                                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                        </div>
                                    </div>
                                    <label for="" class="col-sm-12 control-label">Format Picture : JPG, JPEG, PNG | If you want to change the image click browse</label>
                                </div>
                                    <button type="submit" name="submit" class="btn btn-info">SAVE</button>
                                    <a href="{{ url('/panel/product') }}"><button type="button" class="btn btn-dark">CANCEL</button></a>
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            @endsection