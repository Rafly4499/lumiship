@extends('backend.app')

@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-7 align-self-center">
                        <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Product Services</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb m-0 p-0">
                                    <li class="breadcrumb-item"><a href="{{ url('/panel/dashboard') }}" class="text-muted">Beranda</a></li>
                                    <li class="breadcrumb-item"><a href="{{ url('/panel/product') }}" class="text-muted">Product Services</a></li>
                                    <li class="breadcrumb-item text-muted active" aria-current="page">Insert Data</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Insert Data Product Services</h4>
                                <a href="{{ url('/panel/product') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                                <form class="form-body" method="post" action="{{ url('/panel/product/') }}" enctype="multipart/form-data">
                    @csrf 
                        
                        
                                    <div class="form-group">
                                        <label for="" class="col-sm-2 control-label">Title</label>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control" name="title" required>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Description</label>
                                    <div class="col-md-12">
                                        <textarea class="form-control" rows="3" name="description" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Photo</label>
                                    <div class="col-md-12" style="margin-top: 10px">
                                        <input type="file" class="form-control-file" name="image" required>
                                    </div>
                                    <label for="" class="col-sm-4 control-label">Format Picture : JPG, JPEG, PNG</label>
                                </div>
                                    <button type="submit" name="submit" class="btn btn-info">SUBMIT</button>
                                    <button type="reset" class="btn btn-dark">RESET</button></a>
                                    <input type="hidden" name="_method" value="post"> 
                                </form>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            @endsection