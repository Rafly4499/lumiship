<aside class="left-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar" data-sidebarbg="skin6">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="sidebar-item selected"> <a class="sidebar-link sidebar-link" href="{{ url('/panel/dashboard') }}"
                                aria-expanded="false"><i data-feather="home" class="feather-icon"></i><span
                                    class="hide-menu">Dashboard</span></a></li>
                        <li class="list-divider"></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="{{ url('/panel/about') }}"
                            aria-expanded="false"><i class="icon-grid"></i><span
                                class="hide-menu">Identity Lumiship</span></a></li>
                        <li class="nav-small-cap"><span class="hide-menu">Services</span></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="{{ url('/panel/achievement') }}"
                            aria-expanded="false"><i class="icon-badge"></i><span
                                class="hide-menu">Achievement</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link" href="{{ url('/panel/product') }}"
                                aria-expanded="false"><i class="icon-layers"></i><span
                                    class="hide-menu">Product Services
                                </span></a>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="{{ url('/panel/theory') }}"
                            aria-expanded="false"><i class="icon-notebook"></i><span
                                class="hide-menu">Theory
                            </span></a>
                    </li>
                        <li class="list-divider"></li>
                        <li class="nav-small-cap"><span class="hide-menu">Utilities</span></li>
                        {{-- <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="{{ url('/panel/admin') }}"
                                aria-expanded="false"><i class="icon-social-reddit"></i><span
                                    class="hide-menu">Admin</span></a></li> --}}
                                    <li class="sidebar-item"> <a class="sidebar-link" href="{{ url('/panel/contactus') }}"
                                        aria-expanded="false"><i class="icon-bubbles"></i><span
                                            class="hide-menu">Contact Message</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="{{ url('/panel/logout') }}"
                                aria-expanded="false"><i data-feather="log-out" class="feather-icon"></i><span
                                    class="hide-menu">Logout</span></a></li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>