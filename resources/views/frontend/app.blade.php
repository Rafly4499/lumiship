<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Lumiship is one of logistics company with experience people inside. We trust with efficient distribution, cut the process and always giving more than one solution for your distribution problem.">
    <meta name="author" content="Punggawa Studio">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="keywords" content="Lumiship, cargo, corporate, delivery company, logistic, logistics, moving, packaging, shipment, shipping, storage, transport, transportation, warehouse" />
    <!-- Favicon icon -->
    <title>Lumiship - Logistic</title>
    <!-- Favicons -->
    <link href="{{ asset('frontend/images/favicon.png')}}" rel="icon">

    <!-- Google Fonts -->
    <link rel="stylesheet" href="{{ asset('frontend/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('frontend/css/responsive.css')}}">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
</head>

<body>
    
    <div class="preloader"></div>
    @include('frontend.partials.header')
    <!--  /header  -->

    @yield('content')
    @include('frontend.partials.footer')
    
    <!-- Vendor JS Files -->
    <!-- BACK TO TOP SECTION -->
    <div class="back-to-top bg-primary-color">
        <i class="fab fa-angle-up"></i>
    </div>
    <!-- Integrated important scripts here -->
    <script src="{{ asset('frontend/js/jquery.v1.12.4.min.js')}}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('frontend/js/jquery-core-plugins.js')}}"></script>
    <script src="{{ asset('frontend/js/main.js')}}"></script>
</body>


</html>