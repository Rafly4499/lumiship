<!-- Footer Area Start -->
<!-- Team Section Start -->
<section class="pdt-110 pdb-80" data-background="{{ asset('frontend/images/bg/abs-bg2.png')}}">
    <div class="section-title">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="section-title-left-part title-left-part-primary-color  text-xl-right text-left mb-md-3 mrb-sm-30">
                        <div class="section-left-sub-title mb-20">
                            <h5 class="text-primary-color mrb-15">@lang('footer.theory')</h5>
                        </div>
                        <h2 class="title">@lang('footer.title')</h2>
                    </div>
                </div>
                <div class="col"></div>
                <div class="col-lg-6">
                    <div class="section-title-right-part">
                        <p>@lang('footer.about')</p>
                        <a href="{{ url('/theory') }}" class="cs-btn-one btn-primary-color btn-md">@lang('footer.check')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Team Section End -->
<footer class="footer anim-object2">
    <div class="footer-main-area" data-background="{{ asset('frontend/images/footer-bg.png')}}">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-6 col-md-6">
                    <div class="widget footer-widget">
                        <img width="300px" src="{{ asset('frontend/images/logosamping.png')}}" alt="" class="mrb-20">
                        <address class="mrb-25">
                            <p class="text-light-gray">{{ $about->location }}</p>
                            <div class="mrb-10"><a href="https://wa.me/6281289295897?text=Halo%2C+saya+tertarik+dengan+Lumiship+Logistic" class="text-light-gray"><i class="fas fa-phone-alt mrr-10"></i>+{{ $about->no_hp }}</a></div>
                            <div class="mrb-10"><a href="#" class="text-light-gray"><i class="fas fa-envelope mrr-10"></i>{{ $about->email }}</a></div>
                            <div class="mrb-0"><a href="www.lumiship.co.id" class="text-light-gray"><i class="fas fa-globe mrr-10"></i>www.lumiship.co.id</a></div>
                        </address>
                        <ul class="social-list">
                            <li><a href="{{ $about->link_facebook }}"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="{{ $about->link_instagram }}"><i class="fab fa-instagram"></i></a></li>

                        </ul>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-6 col-md-6">
                    <div class="row">
                        <div class="col-6">
                            <div class="widget footer-widget">
                                <h5 class="widget-title text-white mrb-30">Services</h5>
                                
                                <ul class="footer-widget-list">
                                    @foreach($product as $item)
                                    <li><a href="{{ url('/product/'.$item->slug) }}">{{ $item->title }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="widget footer-widget">
                                <h5 class="widget-title text-white mrb-30">Useful Links</h5>
                                <ul class="footer-widget-list">
                                    <li><a href="{{ url('/') }}">@lang('header.home')</a></li>
										<li><a href="{{ url('/product') }}">@lang('header.product')</a></li>
										<li><a href="{{ url('/theory') }}">@lang('header.theory')</a>
										<li><a href="{{ url('/contactus') }}">@lang('header.contact')</a>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom-area">
        <div class="container footer-border-top pdt-30 pdb-10">
            <div class="row">
                <div class="col-xl-12">
                    <div class="text-center">
                        <span class="text-light-gray">All Rights Reserved by Lumiship. Designed and Developed by <a class="text-primary-color" href="https://punggawastudio.com"> Punggawa Studio</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>