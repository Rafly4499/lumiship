	<!-- header Start -->
	<header class="header-style-two">
		<div class="header-wrapper">
			<div class="header-top-area bg-primary-color d-none d-lg-block">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 header-top-left-part">
							<span class="address"><i class="webexflaticon flaticon-placeholder-1"></i> {{ $about->location }}</span>
							<span class="phone"><i class="webexflaticon flaticon-send"></i> {{ $about->email }}</span>
						</div>
						<div class="col-lg-4 header-top-right-part text-right">
							<ul class="social-links">
								<li><a href="{{ $about->link_facebook }}"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="{{ $about->link_instagram }}"><i class="fab fa-instagram"></i></a></li>
							</ul>
							<div class="language">
								@if(app()->getLocale() == 'en')
								<a class="language-btn" href="#"><i class="webexflaticon flaticon-internet"></i> English</a>
								@elseif(app()->getLocale() == 'id')
								<a class="language-btn" href="#"><i class="webexflaticon flaticon-internet"></i> Indonesia</a>
								@endif
								<ul class="language-dropdown">
									<li><a href="{{ route('localization', 'en') }}" {{ app()->getLocale() == 'en' ? 'active' : '' }}>English</a></li>
									<li><a href="{{ route('localization', 'id') }}" {{ app()->getLocale() == 'id' ? 'active' : '' }}>Indonesia</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="header-navigation-area two-layers-header">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<a class="navbar-brand logo f-left mrt-10 mrt-md-0" href="/">
								<img id="logo-image" class="img-center" src="{{ asset('frontend/images/logosamping.png')}}" alt="">
							</a>
							<div class="mobile-menu-right"></div>
							<div class="header-searchbox-style-two d-none d-xl-block">
								<div class="side-panel side-panel-trigger text-right d-none d-lg-block">
									<span class="bar1"></span>
									<span class="bar2"></span>
									<span class="bar3"></span>
								</div>
							</div>
							<div class="side-panel-content">
								<div class="close-icon">
									<button><i class="webex-icon-cross"></i></button>
								</div>
								<div class="side-panel-logo mrb-30">
									<a href="/">
										<img src="{{ asset('frontend/images/logosamping.png')}}" alt="" />
									</a>
								</div>
								<div class="side-info mrb-30">
									<div class="side-panel-element mrb-25">
										<h4 class="mrb-10">@lang('header.address')</h4>
										<ul class="list-items">
											<li><span class="fa fa-map-marker-alt mrr-10 text-primary-color"></span>{{ $about->location }}</li>
											<li><span class="fas fa-envelope mrr-10 text-primary-color"></span>{{ $about->email }}</li>
											<li><span class="fas fa-phone-alt mrr-10 text-primary-color"></span>{{ $about->no_hp }}</li>
										</ul>
										<a href="https://wa.me/6281289295897?text=Halo%2C+saya+tertarik+dengan+Lumiship+Logistic" class="cs-btn-one"><span class="fas fa-phone alt mrr-10"></span> WhatsApp Now</a>
									</div>
								</div>
								<h4 class="mrb-15">Social List</h4>
								<ul class="social-list">
									<li><a href="{{ $about->link_facebook }}"><i class="fab fa-facebook"></i></a></li>
									<li><a href="{{ $about->link_instagram }}"><i class="fab fa-instagram"></i></a></li>
								</ul>
								
							</div>
							<div class="main-menu f-right">
								<nav id="mobile-menu-right">
									<ul>
										<li><a href="{{ url('/') }}">@lang('header.home')</a></li>
										<li><a href="{{ url('/product') }}">@lang('header.product')</a></li>
										<li><a href="{{ url('/theory') }}">@lang('header.theory')</a>
										<li><a href="{{ url('/contactus') }}">@lang('header.contact')</a>
										
									</ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>