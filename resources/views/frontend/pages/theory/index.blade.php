@extends('frontend.app') 
@section('content')
<!-- Page Title Start -->
<section class="page-title-section">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 text-center">
                <div class="page-title-content">
                    <h3 class="title text-white">All Theory Lumiship</h3>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">All Theory Lumiship</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Page Title End -->
<!-- FAQ Section Start -->
<section class="request-a-call-back pdt-110 pdb-95" data-background="{{ asset('frontend/img/bg/6.html')}}">
    <div class="container">
        <div class="row">
            <div class="col"></div>
            <div class="col-lg-10">
                <div class="faq-block">
                    <div class="accordion">
                    
                        @foreach($product as $item)
                        <div class="accordion-item">
                            <div class="accordion-header active">
                            <h5 class="title">{{ $item->title }}</h5>
                                <span class="fas fa-arrow-right"></span>
                            </div>
                            <div class="accordion-body">
                                <div class="row">
                                @if($item->theory->count())
                                @foreach($item->theory as $result)
                                <div class="col-xl-4 col-lg-6">
                                <div class="testimonial-item">
                                    <h6 class="client-designation mrb-20">{{ $item->title }}</h6>
                                    <h4 class="client-name">{{ $result->title}}</h4>
                                    
                                    <div class="testimonial-content">
                                        <p class="comments">{!! $result->description!!}</p>
                                        <a href="{{ url('upload/'.$result->file) }}" class="cs-btn-one" style="padding: 14px 20px !important;"><span class="far fa-file-pdf mrr-10"></span> Download Theory</a>
                                    </div>
                                </div>
                                </div>
                                @endforeach
                                    
                                @else
                                <h5 class="text-center">No data available</h5>
                                @endif
                                </div>
                            </div>
                        </div>
                        @endforeach 
                    </div>
                </div>
            </div>
            <div class="col"></div>
        </div>
    </div>
</section>
<!-- FAQ Section End -->
@endsection