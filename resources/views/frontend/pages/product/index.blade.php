@extends('frontend.app') 
@section('content')
<!-- Page Title Start -->
<section class="page-title-section">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 text-center">
                <div class="page-title-content">
                    <h3 class="title text-white">All Product Services</h3>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">All Product Services</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Page Title End -->
<!-- Service Content Area Start -->
<section class="service-content-area pdt-110 pdb-80">
    <div class="section-content">
        <div class="container">
            <div class="row justify-content-center">
                @foreach($product as $item)
                <div class="col-xl-4 col-lg-6">
                    <div class="service-item-box mrb-30">
                        <div class="service-item-thumb">
                            <img class="img-full" style="height: 214px !important;" src="{{ asset('img/product/'. $item->photo)}}" alt="">
                            <div class="service-item-icon">
                                <img width="40px" src="{{ asset('frontend/images/logowhite.png')}}" alt="">
                            </div>
                        </div>
                        <div class="service-item-content">
                            <div class="service-item-title">
                            <h3 class="mrb-15">{{ $item->title }}</h3>
                            </div>
                            <div class="service-item-para">
                                <p>{{ Str::limit($item->description, 70) }}</p>
                            </div>
                            <div class="service-item-link">
                                <a class="text-uppercase text-primary-color" href="{{ url('/product/'.$item->slug) }}">Check More</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endsection