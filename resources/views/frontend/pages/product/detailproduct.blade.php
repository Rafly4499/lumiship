@extends('frontend.app') 
@section('content')
    <!-- Service Details Section Start -->
    <section class="page-title-section">
		<div class="container">
			<div class="row">
				<div class="col-xl-12 text-center">
					<div class="page-title-content">
						<h3 class="title text-white">{{ $product->title }}</h3>
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="/">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">{{ $product->title }}</li>
							</ol>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="service-details-page pdt-110 pdb-90">
		<div class="container">
			<div class="row">
				<div class="col-xl-4 col-lg-5">
					<div class="service-nav-menu mrb-30">
						<div class="service-link-list mb-30">
							<ul class="">
                                @foreach($allproduct as $item)
								<li><a href="{{ url('/product/'.$item->slug) }}"><i class="fa fa-chevron-right"></i>{{ $item->title }}</a></li>
								@endforeach
							</ul>
						</div>
					</div>
					<div class="sidebar-widget">
						<div class="brochure-download">
							<h4 class="mrb-40 widget-title">Theory Download</h4>
							<p>Please click download button for getting All theory file</p>
							<a href="{{ url('/theory') }}" class="cs-btn-one"><span class="far fa-file-pdf mrr-10"></span> Download Theory</a>
						</div>
					</div>
					<div class="sidebar-widget bg-primary-color" data-background="{{ asset('frontend/images/bg/abs-bg4.png') }}">
						<div class="contact-information">
							<h3 class="text-white mrb-20">Contact Us</h3>
							<p class="text-white">If you have any query about our service please contact with us</p>
							<ul class="list-items text-white mrb-20">
								<li><span class="fas fa-phone alt mrr-10 text-white"></span>{{ $about->no_hp }}</li>
								<li><span class="fas fa-globe mrr-10 text-white"></span>{{ $about->location }}</li>
								<li><span class="fas fa-envelope mrr-10 text-white"></span>{{ $about->email }}</li>
							</ul>
							<a href="https://wa.me/6281289295897?text=Halo%2C+saya+tertarik+dengan+Lumiship+Logistic" class="cs-btn-one btn-light mrt-15"><span class="fas fa-phone alt mrr-10"></span>WhatsApp Now </a>
						</div>
					</div>
				</div>
				<div class="col-xl-8 col-lg-7">
					<div class="service-detail-text">
						<div class="blog-standared-img slider-blog mrb-35">
							<img class="img-full" src="{{ asset('img/product/'. $product->photo)}}" alt="">
						</div>
						<h3 class="mrb-20">Product Service Details</h3>
                    <p class="mrb-40">{{ $product->description }}</p>
						
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection