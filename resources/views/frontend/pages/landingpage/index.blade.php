@extends('frontend.app') 
@section('content')
<section class="banner-section">
    <div class="home-carousel owl-theme owl-carousel">
        <div class="slide-item">
            <div class="image-layer" data-background="{{ asset('frontend/images/bg/1.jpg')}}"></div>
            <div class="auto-container">
                <div class="row clearfix">
                    <div class="col-xl-12 col-lg-12 col-md-12 content-column">
                        <div class="content-box">
                            <h1>@lang('home.banner1_title')</h1>
                            <p>@lang('home.banner1_desc')</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide-item">
            <div class="image-layer" data-background="{{ asset('frontend/images/bg4.jpg')}}"></div>
            <div class="auto-container">
                <div class="row clearfix">
                    <div class="col-xl-12 col-lg-12 col-md-12 content-column text-center">
                        <div class="content-box">
                            <h1>@lang('home.banner2_title')</h1>
                            <p>@lang('home.banner2_desc')</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide-item">
            <div class="image-layer" data-background="{{ asset('frontend/images/bg/3.jpg')}}"></div>
            <div class="auto-container">
                <div class="row clearfix">
                    <div class="col-xl-12 col-lg-12 col-md-12 content-column">
                        <div class="content-box">
                            <h1>@lang('home.banner3_title')</h1>
                            <p>@lang('home.banner3_desc')</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Home Slider End -->
<!-- About Title Section Start -->
<section class="about-section bg-silver-light pdt-110 pdb-100">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xl-6">
                <div class="about-us-wrapper">
                    <h6 class="text-primary-color side-line-left mrb-15">@lang('home.aboutus_title')</h6>
                    <h2>@lang('home.aboutus_desc')</h2>
                </div>
            </div>
            <div class="col-md-12 col-xl-6">
            <p class="content-border-left mrt-30">@lang('home.aboutus_content')
            </p>
            </div>
        </div>
    </div>
</section>
<!-- About Title Section End -->
<!-- About Section Start -->
<section class="about-section anim-object pdt-0 pdb-170 pdb-lg-110" data-background="{{ asset('frontend/images/bg/abs-bg1.png')}}">
    <div class="container">
        <div class="row mrb-80 align-items-center">
            <div class="col-md-12 col-xl-6">
                <div class="row mrt-110">
                    <div class="col-md-6 col-xl-6">
                        <div class="about-feature-box">
                            <div class="about-feature-box-icon">
                                <span class="webexflaticon flaticon-delivery-truck-1"></span>
                            </div>
                            <div class="about-feature-box-content">
                                <div class="title">
                                    <a href="#">
                                        <h3>@lang('home.features1_title')</h3>
                                    </a>
                                </div>
                                <div class="para">
                                    <p>@lang('home.features1_desc')</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xl-6">
                        <div class="about-feature-box">
                            <div class="about-feature-box-icon">
                                <span class="webexflaticon flaticon-globe"></span>
                            </div>
                            <div class="about-feature-box-content">
                                <div class="title">
                                    <a href="#">
                                        <h3>@lang('home.features2_title')</h3>
                                    </a>
                                </div>
                                <div class="para">
                                    <p>@lang('home.features2_desc')</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-12">
                <div class="about-image-block mrt--110 mrt-lg-0 mrr-30 mrr-lg-0">
                    <img class="img-full js-tilt" src="{{ asset('frontend/images/project/project_04.jpg')}}" alt="">
                </div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-md-12 col-xl-6">
                <div class="about-image-box mrr-60 mrr-lg-0 mrb-lg-110">
                    <img class="about-image1 img-full js-tilt d-none d-xl-block" src="{{ asset('frontend/images/about/ab2.jpg')}}" alt="">
                    <img class="about-image2 img-full" src="{{ asset('frontend/images/project/project_01.jpg')}}" alt="">
                    <div class="experience">
                        <h4>10+</h4>
                        <p>@lang('home.experience_desc')</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-xl-6 pdl-60">
                <h5 class="side-line-left text-primary-color mrb-15">@lang('home.about_company')</h5>
                <h2 class="text-uppercase mrb-30">@lang('home.about_desc')</h2>
                <p class="mrb-40">{{ $about->about }}</p>
                <div class="d-inline d-md-flex align-items-center mt-40">
                    <div class="signature">
                        <img src="{{ asset('frontend/signature.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About Section End -->
<!-- Service Titile Area Start -->
<section class="bg-primary-color pdt-110 pdb-150">
    <div class="section-title mrb-0">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="section-title-left-part text-xl-right text-left mrb-20 mrb-sm-30">
                        <div class="section-left-sub-title mb-20">
                            <h5 class="sub-title text-white">@lang('home.service_title')</h5>
                        </div>
                        <h2 class="title text-white">@lang('home.service_subtitle')</h2>
                    </div>
                </div>
                <div class="col"></div>
                <div class="col-lg-6">
                    <div class="section-title-right-part">
                        <p class="text-white">@lang('home.service_desc')</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Service Titile Area End -->
<!-- Service Content Area Start -->
<section class="service-content-area pdb-200">
    <div class="section-content">
        <div class="container">
            <div class="row justify-content-center">
                @foreach($product as $item)
                <div class="col-xl-4 col-lg-6">
                    <div class="service-item-box mrt--110 mrb-md-0">
                        <div class="service-item-thumb">
                            <img class="img-full" style="height: 214px !important;" src="{{ asset('img/product/'. $item->photo)}}" alt="">
                            <div class="service-item-icon">
                                <img width="40px" src="{{ asset('frontend/images/logowhite.png')}}" alt="">
                            </div>
                        </div>
                        <div class="service-item-content">
                            <div class="service-item-title">
                            <h3 class="mrb-15">{{ $item->title }}</h3>
                            </div>
                            <div class="service-item-para">
                                <p>{{ Str::limit($item->description, 70) }}</p>
                            </div>
                            <div class="service-item-link">
                                <a class="text-uppercase text-primary-color" href="{{ url('/product/'.$item->slug) }}">@lang('home.explore_btn')</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            
            <div class="row mrt-60">
                <div class="col-xl-12 text-center">
                    <h5>@lang('home.explore_more') <span><a href="{{ url('/product') }}" class="text-underline text-primary-color">@lang('home.explore_click')</a></span></h5>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Service Content Area End -->
<!-- Divider Section Start -->
<section class="person-object pdt-110 pdb-80" data-background="{{ asset('frontend/images/bg/1.jpg')}}" data-overlay-dark="1">
    <div class="section-content">
        <div class="container">
            <div class="row">
                @if ($message = Session::get('success'))
                <script>
                    Swal.fire({
                        title: 'Success!',
                        text:  'Your message was sent successfully. Lumiship will contact you shortly.',
                        icon: 'success',
                        confirmButtonText: 'Ok'
                      })
                    </script>
                @endif
                @if ($message = Session::get('error'))
                <script>
                    Swal.fire({
                        title: 'Failed!',
                        text:  'Your message failed. Check Your Field Input',
                        icon: 'error',
                        confirmButtonText: 'Ok'
                      })
                    </script>
                @endif
                <div class="col-lg-6 col-xl-5 mrb-sm-110 mrb-lg-30">
                    <div class="request-a-call-back-form bg-white mrt--235 mrl--160">
                        <h2 class="mrt-0 mrb-40 solid-side-line">@lang('home.contactus')</h2>
                        <p>Contact us <a href="https://wa.me/6281289295897?text=Halo%2C+saya+tertarik+dengan+Lumiship+Logistic">via WhatsApp </a> or the form below</p>
                        
                        <form method="post" action="{{ url('/contactus') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" placeholder="Name" name="name"class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="number" placeholder="Phone" name="phone_number" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="email" placeholder="Email" name="email" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" placeholder="Subject" name="subject" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <textarea name="message" id="message" class="form-control" placeholder="message"></textarea>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group mrb-30 mt-5">
                                        <button type="submit" class="cs-btn-one btn-primary-color btn-md">@lang('home.contactus_submit')</button>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="_method" value="post">
                        </form>
                    </div>
                </div>
                <div class="col-lg-12 col-xl-7">
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-xl-6">
                            <div class="funfact js-tilt mrb-30">
                                <div class="icon">
                                    <span class="webexflaticon flaticon-delivery-truck-1"></span>
                                </div>
                                <h2 class="counter">{{ $achieve->delivered_packages }}</h2>
                                <h5 class="title">@lang('home.achieve_delivered')</h5>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xl-6">
                            <div class="funfact js-tilt mrb-30">
                                <div class="icon">
                                    <span class="webexflaticon flaticon-globe"></span>
                                </div>
                                <h2 class="counter">{{ $achieve->countries }}</h2>
                                <h5 class="title">@lang('home.achieve_countries')</h5>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xl-6">
                            <div class="funfact js-tilt mrb-30">
                                <div class="icon">
                                    <span class="webexflaticon flaticon-building"></span>
                                </div>
                                <h2 class="counter">{{ $achieve->project }}</h2>
                                <h5 class="title">@lang('home.achieve_project')</h5>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xl-6">
                            <div class="funfact js-tilt mrb-30">
                                <div class="icon">
                                    <span class="webexflaticon flaticon-man-2"></span>
                                </div>
                                <h2 class="counter">{{ $achieve->client }}</h2>
                                <h5 class="title">@lang('home.achieve_client')</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col"></div>
            </div>
        </div>
    </div>
</section>
<!-- Divider Section End -->


@endsection