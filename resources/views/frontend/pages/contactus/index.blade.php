@extends('frontend.app') 
@section('pageTitle', 'Contact Us')
@section('content')
<!-- Page Title Start -->
<section class="page-title-section">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 text-center">
                <div class="page-title-content">
                    <h3 class="title text-white">Contact Us</h3>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index-2.html">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Page Title End -->
<!-- Contact Section Start -->
<section class="contact-section pdt-110 pdb-95 pdb-lg-90" data-background="{{ asset('frontend/images/bg/abs-bg1.png')}}">
    <div class="container" style="margin-bottom:30px;">
        <div class="row" >
            <div class="col-lg-5">
                <div class="section-title-left-part title-left-part-primary-color  text-xl-right text-left mb-md-3 mrb-sm-30">
                    <div class="section-left-sub-title mb-20">
                        <h5 class="text-primary-color mrb-15">Contact Your Request</h5>
                    </div>
                    <h2 class="title">Contact Us</h2>
                    <p>Contact us via WhatsApp or the form below</p>
                </div>
            </div>
            <div class="col"></div>
            <div class="col-lg-6">
                <div class="section-title-right-part" style="padding-top: 30px;">
                   
                    <a href="https://wa.me/6281289295897?text=Halo%2C+saya+tertarik+dengan+Lumiship+Logistic" class="cs-btn-one btn-primary-color btn-md">Call Via WhatsApp</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mrb-60">
            @if ($message = Session::get('success'))
                <script>
                    Swal.fire({
                        title: 'Success!',
                        text:  'Your message was sent successfully. Lumiship will contact you shortly.',
                        icon: 'success',
                        confirmButtonText: 'Ok'
                      })
                    </script>
                @endif
                @if ($message = Session::get('error'))
                <script>
                    Swal.fire({
                        title: 'Failed!',
                        text:  'Your message failed. Check Your Field Input',
                        icon: 'error',
                        confirmButtonText: 'Ok'
                      })
                    </script>
                @endif
            <div class="col-lg-7">
                <div class="contact-form">
                    <form method="post" action="{{ url('/sharemessage') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mrb-25">
                                    <input type="text" placeholder="Name" name="name"class="form-control" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mrb-25">
                                    <input type="text" placeholder="Phone" name="phone_number" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mrb-25">
                                    <input type="email" placeholder="Email" name="email" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group mrb-25">
                                    <input type="text" placeholder="Subject" name="subject" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group mrb-25">
                                <textarea name="message" id="message" class="form-control" placeholder="message"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <button type="submit" class="cs-btn-one btn-md btn-round btn-primary-color element-shadow">Submit Now</button>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="_method" value="post">
                    </form>
                </div>
            </div>
            <div class="col-lg-5">
                <!-- Google Map Start -->
                <div class="mapouter fixed-height">
                    <div class="gmap_canvas">
                        <iframe id="gmap_canvas" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3958.003877367837!2d112.73148241530332!3d-7.240394673125528!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7f93c7068f4b3%3A0x562ba45f72dfece7!2sJl.%20Krembangan%20Barat%20No.11%2C%20RT.000%2FRW.09%2C%20Krembangan%20Sel.%2C%20Kec.%20Krembangan%2C%20Kota%20SBY%2C%20Jawa%20Timur%2060175!5e0!3m2!1sen!2sid!4v1609812898039!5m2!1sen!2sid"></iframe>
                    </div>
                </div>
                <!-- Google Map End -->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-xl-4">
                <div class="contact-block d-flex mrb-30" style="height:178px;">
                    <div class="contact-icon">
                        <i class="webex-icon-map1"></i>
                    </div>
                    <div class="contact-details mrl-30">
                        <h5 class="icon-box-title mrb-10">Our Address</h5>
                    <p class="mrb-0"><a href="https://www.google.co.id/maps/place/Jl.+Krembangan+Barat+No.11,+RT.000%2FRW.09,+Krembangan+Sel.,+Kec.+Krembangan,+Kota+SBY,+Jawa+Timur+60175/@-7.2404,112.7314824,17z/data=!3m1!4b1!4m5!3m4!1s0x2dd7f93c7068f4b3:0x562ba45f72dfece7!8m2!3d-7.2404!4d112.7336711">{{ $about->location }}</a></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-xl-4">
                <div class="contact-block d-flex mrb-30" style="height:178px;">
                    <div class="contact-icon">
                        <i class="webex-icon-Phone2"></i>
                    </div>
                    <div class="contact-details mrl-30">
                        <h5 class="icon-box-title mrb-10">Phone Number</h5>
                        <p class="mrb-0"><a href="https://wa.me/6281289295897?text=Halo%2C+saya+tertarik+dengan+Lumiship+Logistic">{{ $about->no_hp }}</a></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-xl-4">
                <div class="contact-block d-flex mrb-30" style="height:178px;">
                    <div class="contact-icon">
                        <i class="webex-icon-envelope"></i>
                    </div>
                    <div class="contact-details mrl-30">
                        <h5 class="icon-box-title mrb-10">Email Us</h5>
                        <p class="mrb-0"><a href="mailto:{{ $about->email }}">{{ $about->email }}</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection