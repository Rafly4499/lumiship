<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contactus extends Model
{
    protected $table = "tb_contactus";
    protected $fillable= [
        'name', 'phone_number', 'subject', 'message', 'email',
    ];
}
