<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product  extends Model
{
    protected $table = "product";
    protected $fillable= [
        'title', 'photo', 'description', 'slug'
    ];
    public function theory(){
        return $this->hasMany(Theory::class,'product_id');
    }
}
