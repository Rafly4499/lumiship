<?php

namespace App\Http\Controllers;

use App\Theory;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Image;
use File;
class TheoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $theory = Theory::all();
        return view('backend.pages.theory.index', compact('theory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Theory::orderBy('id')->get();
        $theory = Theory::all();
        $category = Product::all();
        return view('backend.pages.theory.create', compact('theory','category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'title_theory' => 'required',
        //     'content_theory' => 'required',
        //     'photo_theory' => 'required',
        //     'tgl_posting' => 'required',
        //     'id_category_theory' => 'required',
        //     'id_user' => 'required',
        //     'slug' => 'required',
            
            
        // ]);
        $request->validate([
            'file' => 'required|mimes:pdf,xlx,csv|max:2048',
        ],
        ['file.required' => 'the file entered does not comply with the provisions']
      );
        $data = $request->all();
        $data['slug'] = Str::slug($request->get('title'));
        if ($request->hasFile('file')) {
            if ($request->file('file')->isValid()) {
                $destinationPath = 'upload'; // upload path
                $extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
                $fileName = $request->title.'.'.$extension; // renaming image
                $request->file('file')->move($destinationPath, $fileName); // uploading file to given path
                $data['file'] = $fileName;
            }
          }

    	$theory = Theory::create($data);
        Session::flash('success', $data['title'] . ' added successfully');
        return redirect('/panel/theory');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\theory  $theory
     * @return \Illuminate\Http\Response
     */
    public function show(theory $theory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\theory  $theory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $theory = Theory::find($id);
        $category = Product::all();
        return view('backend.pages.theory.edit', compact('theory','category'));
    }
    public function view($id)
    {
        $theory = Theory::find($id);
        $category = Product::all();
        return view('backend.pages.theory.view', compact('theory','category'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\theory  $theory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
        'file' => 'required|mimes:pdf,xlx,csv|max:2048',
    ],
    ['file.required' => 'the file entered does not comply with the provisions']
  );
        $req = $request->except('_method', '_token', 'submit');
        if ($request->hasFile('file')) {
            if ($request->file('file')->isValid()) {
              $destinationPath = 'upload/'; // upload path
            $extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
            $fileName = $request->title.'.'.$extension; // renaming image
            $request->file('file')->move($destinationPath, $fileName); // uploading file to given path
            $req['file'] = $fileName;
    
              $result = Theory::find($id);
              if (!empty($result->file)) {
                File::delete('upload/'.$result->file);
              }
            }else {
              unset($req['file']);
            }
          }else {
            unset($req['file']);
          }
          $req['slug'] = Str::slug($req['title']);
          $data = Theory::where('id', $id)->update($req);
	    Session::flash('success', $data['title'] . ' updated successfully');
        return redirect('panel/theory');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\theory  $theory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Theory::find($id);
	    $data->destroy($id);

	    Session::flash('success', $data['title'] . ' deleted successfully');
	    return redirect('panel/theory');
    }
}
