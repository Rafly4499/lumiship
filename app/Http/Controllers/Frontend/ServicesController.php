<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactEmail;
use App\Contactus;
use App\Product;
use App\Theory;
use App\AboutLumiship;
class ServicesController extends Controller
{
    public function product(){
        $product = Product::all();
        return view('frontend.pages.product.index',compact('product'));
    }
    public function detailProduct($slug){
        $product = Product::where('slug', $slug)->first();
        $allproduct = Product::all();
        $theory = Theory::where('product_id',$product->id)->first();
        $about = AboutLumiship::first();
        return view('frontend.pages.product.detailproduct',compact(
            'theory',
            'product',
            'allproduct',
            'about'
            
        ));
    }
    public function theory(){
        $product = Product::with('theory')->get();
        $theory = Theory::get();
        return view('frontend.pages.theory.index',compact('product','theory'));   
    }
    public function contactus(){
        $about = AboutLumiship::first();
        return view('frontend.pages.contactus.index',compact('about'));   
    }
    public function sendMessage(Request $request)
    {
       $data = $request->all();
       $contact = ContactUs::create($data);
    //    $messages = ContactUs::where('id', $contact->id)->first();
    //    if ($messages) {
    //        try{
    //            Mail::to($messages->email)->send(new ContactEmail( $messages ));
    //            Session::flash('success', 'Your message was sent successfully. Lumiship will contact you shortly.');
    //            return redirect('/');
    //        }catch (Exception $e){
    //            Session::flash('error', 'Your request failed');
    //            return redirect('/');
    //        }
    //        Session::flash('success', 'Your message was sent successfully. Lumiship will contact you shortly.');
    //        return redirect('/');
    //    } else {
    //        Session::flash('error', 'Your request failed');
    //        return redirect()->back();
    //    }
    Session::flash('success', 'Your message was sent successfully. Lumiship will contact you shortly.');
    return redirect('/');
    }
}
