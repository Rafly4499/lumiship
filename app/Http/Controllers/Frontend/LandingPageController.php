<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactEmail;
use App\Contactus;
use App\Achievement;
use App\Product;
use App\Theory;
use App\AboutLumiship;
class LandingPageController extends Controller
{
    public function index(){
        $theory = Theory::first();
        $product = Product::limit(3)->get();
        $achieve = Achievement::first();
        $about = AboutLumiship::first();
        $contact = ContactUs::all()->count();
        return view('frontend.pages.landingpage.index',compact(
            'theory',
            'product',
            'achieve',
            'contact',
            'about'
            
        ));
    }
    public function contact(Request $request)
     {
        $data = $request->all();
	    $contact = ContactUs::create($data);
        // $messages = [
        //     'name'          => $request->name,
        //     'email'         => $request->email,
        //     'phone_number'  => $request->phone_number,
        //     'subject'       => $request->subject,
        //     'message'       => $request->message
        // ];
        //     try{
        //         Mail::to('cs@lumiship.co.id')->send(new ContactEmail( $messages ));
        //         Session::flash('success', 'Your message was sent successfully. Lumiship will contact you shortly.');
        //         return redirect('/');
        //     }catch (Exception $e){
        //         Session::flash('error', 'Your request failed');
        //         return redirect('/');
        //     }
            Session::flash('success', 'Your message was sent successfully. Lumiship will contact you shortly.');
            return redirect('/');
     }
     public function switch($language = '')
     {
         // Simpan locale ke session.
         request()->session()->put('locale', $language);
  
         // Arahkan ke halaman sebelumnya.
         return redirect()->back();
     }
}
