<?php

namespace App\Http\Controllers;

use App\AboutLumiship;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
class AboutLumishipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = AboutLumiship::first();
        return view('backend.pages.aboutlumiship.index', compact('item'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = AboutLumiship::orderBy('id')->get();
        $about = AboutLumiship::all();
        return view('backend.pages.aboutlumiship.create', compact('about'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
	    AboutLumiship::create($data);
	    Session::flash('message', ' added successfully');
	    return redirect('/panel/about');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AboutLumiship  $aboutlumiship
     * @return \Illuminate\Http\Response
     */
    public function show(aboutlumiship $aboutlumiship)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AboutLumiship  $aboutlumiship
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = AboutLumiship::find($id);
        return view('backend.pages.aboutlumiship.edit', ['about' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AboutLumiship  $aboutlumiship
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = AboutLumiship::find($id);
        $about = $request->all();
        $data->update($about);

	    Session::flash('success', ' updated successfully');
        return redirect('/panel/about');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AboutLumiship  $aboutlumiship
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = AboutLumiship::find($id);
	    $data->destroy($id);

	    Session::flash('success', ' deleted successfully');
	    return redirect('/panel/about');
    }
}
