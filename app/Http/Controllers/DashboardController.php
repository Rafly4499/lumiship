<?php

namespace App\Http\Controllers;

use App\Achievement;
use App\ContactUs;
use App\Product;
use App\Theory;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $counttheory = Theory::all()->count();
        $countproduct = Product::all()->count();
        $achieve = Achievement::first();
        $countcontact = ContactUs::all()->count();
        $client->useApplicationDefaultCredentials();
        if ($client->isAccessTokenExpired()) {
            $client->refreshTokenWithAssertion();
        }
        $arrayInfo = $client->getAccessToken();
        $accesstoken = $arrayInfo['access_token'];
            return view('backend.pages.dashboard',compact('counttheory', 'countcontact', 'countproduct', 'achieve', '$accesstoken'));
        }
}
