<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class AdminController extends Controller
{
    public function index()
    {
        $admin = User::all()->where('is_admin','=','1');
        return view('backend.pages.admin.index', compact('admin'));
    }
}
