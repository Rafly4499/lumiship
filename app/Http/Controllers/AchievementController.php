<?php

namespace App\Http\Controllers;

use App\Achievement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
class AchievementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $achievement = Achievement::all();
        return view('backend.pages.achievement.index', compact('achievement'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Achievement::orderBy('id')->get();
        $achievement = Achievement::all();
        return view('backend.pages.achievement.create', compact('achievement'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
	    Achievement::create($data);
	    Session::flash('message', ' added successfully');
	    return redirect('/panel/achievement');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Achievement  $Achievement
     * @return \Illuminate\Http\Response
     */
    public function show(Achievement $Achievement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Achievement  $Achievement
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Achievement::find($id);
        return view('backend.pages.achievement.edit', ['achievement' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Achievement  $Achievement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Achievement::find($id);
        $achievement = $request->all();
        $data->update($achievement);

	    Session::flash('success', ' updated successfully');
        return redirect('/panel/achievement');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Achievement  $Achievement
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Achievement::find($id);
	    $data->destroy($id);

	    Session::flash('success', ' deleted successfully');
	    return redirect('/panel/achievement');
    }
}
