<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Theory;
use App\ContactUs;
use App\Product;
use App\Achievement;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('frontend.app');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
     public function adminHome()
     {
        $counttheory = Theory::all()->count();
        $countproduct = Product::all()->count();
        $achieve = Achievement::first();
        $countcontact = ContactUs::all()->count();
        $client->useApplicationDefaultCredentials();
        if ($client->isAccessTokenExpired()) {
            $client->refreshTokenWithAssertion();
        }
        $arrayInfo = $client->getAccessToken();
        $accesstoken = $arrayInfo['access_token'];
            return view('backend.pages.dashboard',compact('counttheory', 'countcontact', 'countproduct', 'achieve', '$accesstoken'));
        }  
}
