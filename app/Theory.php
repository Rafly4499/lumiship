<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Theory extends Model
{
    protected $table = "theory";
    protected $fillable= [
        'title', 'file', 'description', 'slug', 'product_id'
    ];
    public function category(){
        return $this->belongsTo(Product::class,'product_id');
    }
}
