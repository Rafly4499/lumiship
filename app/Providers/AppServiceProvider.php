<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use App\AboutLumiship;
use App\Product;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('frontend.partials.footer', function($view) {
            $view->with('product', Product::limit(8)->get());
            $view->with('about', AboutLumiship::first());
         });
        view()->composer('frontend.partials.header', function($view) {
            $view->with('about', AboutLumiship::first());
         });
    }
}
