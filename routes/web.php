<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('frontend.app');
});


Auth::routes();
Route::group(['prefix' => '/panel', 'middleware' => 'is_admin'], function () {
    Route::get('/', 'HomeController@adminHome')->name('admin.home')->middleware('is_admin');
    Route::get('/dashboard', 'DashboardController@index');
    Route::resource('/product', 'PartListController');
    Route::resource('/admin', 'AdminController');
    Route::resource('/achievement', 'AchievementController');
    Route::resource('/about', 'AboutLumishipController');
    Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
    Route::resource('/theory', 'TheoryController');
    Route::get('/theory', 'TheoryController@index');
    Route::post('/theory', 'TheoryController@store');
    Route::get('/theory/{id}/view', 'TheoryController@view');

    Route::resource('/contactus', 'ContactusController');
    Route::get('/contactus', 'ContactusController@index');
    Route::get('/contactus/{id}/view', 'ContactusController@view');
    Route::get("google-analytics-summary",array("as"=>"google-analytics-summary","uses"=>"DashboardController@getAnalyticsSummary"));
});

// Frontend
Route::get('/','Frontend\LandingPageController@index');
Route::get( '/theory', 'Frontend\TheoryController@index')->name('theory');
Route::get( '/theory/{slug}', 'Frontend\TheoryController@detailTheory')->name('detail');
Route::get( '/theory/category/{slug}', 'Frontend\TheoryController@categoryTheory' )->name('category');
Route::get( '/searchtheory', 'Frontend\TheoryController@searchTheory' )->name('searchtheory');


Route::post('/contactus', 'Frontend\LandingPageController@contact');
Route::get('/contactus', 'Frontend\ServicesController@contactus');
Route::get('/product', 'Frontend\ServicesController@product');
Route::get('/product/{slug}', 'Frontend\ServicesController@detailProduct');
Route::get('/theory', 'Frontend\ServicesController@theory');
Route::post('/sharemessage', 'Frontend\ServicesController@shareMessage');
Route::get('lang/{language}', 'Frontend\LandingPageController@switch')->name('localization');