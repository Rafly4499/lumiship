<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutLumishipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('vission');
            $table->text('mission');
            $table->text('about');
            $table->text('location');
            $table->text('email');
            $table->string('contact_person');
            $table->text('no_hp');
            $table->string('link_embed_map');
            $table->string('link_video_profile');
            $table->string('link_facebook');
            $table->string('link_instagram');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_lumiship');
    }
}
